#include <cstdlib>
#include "Enumerations.h"
#include "Piece.h"
#include "ChessPiece.h"
#include <vector>

int Pawn::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
        if (start.x != end.x) {
          return -1;
        }
        if (_owner == Player::WHITE) {
          if (start.y == 1) {
            if ((int) end.y - (int) start.y == 2) {
              trajectory.push_back(Position(start.x, start.y + 1));
              return 1;
            }
          }
          if ((int) end.y - (int) start.y == 1) {
            return 1;
          }
        } else {
          if (start.y == 6) {
            if ((int) end.y - (int) start.y == -2) {
              trajectory.push_back(Position(start.x, start.y - 1));
              return 1;
            }
          }
          if ((int) end.y - (int) start.y == -1) {
            return 1;
          }
        }
        return -1;
}


int Rook::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
        if (start.x == end.x && end.y != start.y) {
          if (end.y > start.y) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
              trajectory.push_back(Position(start.x, i));
            }
          } else {
            for (unsigned int i = end.y + 1; i < start.y; i++) {
              trajectory.push_back(Position(start.x, i));
            }
          }
          return 1;
        } else if (start.y == end.y && start.x != end.x) {
          if (end.x > start.x) {
            for (unsigned int i = start.x + 1; i < end.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          } else {
            for (unsigned int i = end.x + 1; i < start.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          }
          return 1;
        }
        return -1;
}



int Knight::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
        trajectory.clear();
        if ((abs((int) start.x - (int) end.x) == 1 && abs((int) start.y - (int) end.y) == 2) ||
            (abs((int) start.x - (int) end.x) == 2 && abs((int) start.y - (int) end.y) == 1)) {
          return 1;
        }
        return -1;
}


int Bishop::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
        if (abs((int) start.x - (int) end.x) == abs((int) start.y - (int) end.y)  && start.x != end.x) {
          if (((int) end.x - (int) start.x) > 0) {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y - i));
              }
            }
          } else {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y - i));
              }
            }
          }
          return 1;
        }
        return -1;
}


int Queen::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
        if (abs((int) start.x - (int) end.x) == abs((int) start.y - (int) end.y) && start.x != end.x) {
          if (((int) end.x - (int) start.x) > 0) {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y - i));
              }
            }
          } else {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y - i));
              }
            }
          }
          return 1;
        } else if (start.x == end.x && start.y != end.y) {
          if (end.y > start.y) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
              trajectory.push_back(Position(start.x, i));
	    }
          } else {
            for (unsigned int i = end.y + 1; i < start.y; i++) {
              trajectory.push_back(Position(start.x, i));
	    }
          }
          return 1;
        } else if (start.y == end.y && start.x != end.x) {
          if (end.x > start.x) {
            for (unsigned int i = start.x + 1; i < end.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          } else {
            for (unsigned int i = end.x + 1; i < start.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          }
          return 1;
        }
        return -1;
}


int King::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
      trajectory.clear();
      if ((abs((int) start.x - (int) end.x) == 1 && abs((int) start.y - (int) end.y) <= 1) || (abs(start.y - end.y) == 1 && abs((int) start.x - (int) end.x) <= 1)) {
          return 1;
        }
        return -1;
}


int Ghost::valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const {
      start = start;
      end = end;
      trajectory.clear();
      return -1;
}



