#ifndef CHESS_PIECE_H
#define CHESS_PIECE_H

#include <cstdlib>
#include "Enumerations.h"
#include "Piece.h"
#include <vector>

class Pawn : public Piece {

protected:
    friend PieceFactory<Pawn>;
    Pawn(Player owner, int piece_type) : Piece(owner, piece_type) {}

public:
    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override;
/**        if (start.x != end.x) {
          return -1;:x
        }
        if (_owner == Player::WHITE) {
          if (start.y == 1) {
            if ((int) end.y - (int) start.y == 2) {
              trajectory.push_back(Position(start.x, start.y + 1));
              return 1;
            }
          }
          if ((int) end.y - (int) start.y == 1) {
            return 1;
          }
        } else {
          if (start.y == 6) {
            if ((int) end.y - (int) start.y == -2) {
              trajectory.push_back(Position(start.x, start.y - 1));
              return 1;
            }
          }
          if ((int) end.y - (int) start.y == -1) {
            return 1;
          }
        }
        return -1;
    }
*/
};


class Rook : public Piece {
  
 protected:
  friend PieceFactory<Rook>;
 Rook(Player owner, int piece_type) : Piece(owner, piece_type) {}
  
 public:
  // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override; 
/**
        if (start.x == end.x && end.y != start.y) {
          if (end.y > start.y) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
              trajectory.push_back(Position(start.x, i));
            }
          } else {
            for (unsigned int i = end.y + 1; i < start.y; i++) {
              trajectory.push_back(Position(start.x, i));
            }
          }
          return 1;
        } else if (start.y == end.y && start.x != end.x) {
          if (end.x > start.x) {
            for (unsigned int i = start.x + 1; i < end.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          } else {
            for (unsigned int i = end.x + 1; i < start.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          }
          return 1;
        }
        return -1;
    }
*/
};



class Knight : public Piece {

protected:
    friend PieceFactory<Knight>;
    Knight(Player owner, int piece_type) : Piece(owner, piece_type) {}

public:
    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override;
/**
        trajectory.clear();
        if ((abs((int) start.x - (int) end.x) == 1 && abs((int) start.y - (int) end.y) == 2) ||
            (abs((int) start.x - (int) end.x) == 2 && abs((int) start.y - (int) end.y) == 1)) {
          return 1;
        }
        return -1;
    }
*/
};


class Bishop : public Piece {

protected:
    friend PieceFactory<Bishop>;
    Bishop(Player owner, int piece_type) : Piece(owner, piece_type) {}

public:
    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override;
/**
        if (abs((int) start.x - (int) end.x) == abs((int) start.y - (int) end.y)  && start.x != end.x) {
          if (((int) end.x - (int) start.x) > 0) {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y - i));
              }
            }
          } else {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y - i));
              }
            }
          }
          return 1;
        }
        return -1;
    }
*/
};


class Queen : public Piece {

protected:
    friend PieceFactory<Queen>;
    Queen(Player owner, int piece_type) : Piece(owner, piece_type) {}

public:
    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override;
/**
        if (abs((int) start.x - (int) end.x) == abs((int) start.y - (int) end.y) && start.x != end.x) {
          if (((int) end.x - (int) start.x) > 0) {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (end.x -start.x);  i++) {
                trajectory.push_back(Position(start.x + i, start.y - i));
              }
            }
          } else {
            if (((int) end.y - (int) start.y) > 0) {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y + i));
              }
            } else {
              for (unsigned int i = 1; i < (start.x - end.x);  i++) {
                trajectory.push_back(Position(start.x - i, start.y - i));
              }
            }
          }
          return 1;
        } else if (start.x == end.x && start.y != end.y) {
          if (end.y > start.y) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
              trajectory.push_back(Position(start.x, i));
	    }
          } else {
            for (unsigned int i = end.y + 1; i < start.y; i++) {
              trajectory.push_back(Position(start.x, i));
	    }
          }
          return 1;
        } else if (start.y == end.y && start.x != end.x) {
          if (end.x > start.x) {
            for (unsigned int i = start.x + 1; i < end.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          } else {
            for (unsigned int i = end.x + 1; i < start.x; i++) {
              trajectory.push_back(Position(i, start.y));
            }
          }
          return 1;
        }
        return -1;
    }
*/
};


class King : public Piece {

protected:
    friend PieceFactory<King>;
    King(Player owner, int piece_type) : Piece(owner, piece_type) {}

public:
    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override;
/**
      trajectory.clear();
      if ((abs((int) start.x - (int) end.x) == 1 && abs((int) start.y - (int) end.y) <= 1) || (abs(start.y - end.y) == 1 && abs((int) start.x - (int) end.x) <= 1)) {
          return 1;
        }
        return -1;
    }
*/

};

//Piece only used in SpookyChess
class Ghost : public Piece {

protected:
    friend PieceFactory<Ghost>;
    Ghost(Player owner, int piece_type) : Piece(owner, piece_type) {}

public:
    // Returns an integer representing move shape validity
    // where a value >= 0 means valid, < 0 means invalid.
    // also populates a vector of Positions with the trajectory
    // followed by the Piece from start to end
    virtual int valid_move_shape(Position start, Position end, std::vector<Position>& trajectory) const override;
/**
      start = start;
      end = end;
      trajectory.clear();
      return -1;
    }
*/
};


#endif // CHESS_PIECE_H
